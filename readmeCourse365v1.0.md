## Project Course
## Description
### ** Tạo trang bán khóa học, chức năng thêm mới, sửa, xóa 1 khóa học
## Feature
+ Xây dựng menu và slice content
![Build](images/01_tao_khoi_menu_va_slice_content.png)
+ Xây dựng course content
![Build](images/02_tao_khoi_course_content.png)
+ Thêm mới 1 khóa học
![Create](images/03_chuc_nang_them_moi_khoa_hoc.png)
+ Sửa 1 khóa học
![Update](images/04_chuc_nang_sua_khoa_hoc.png)
+ Xóa 1 khóa học
![Delete](images/05_chuc_nang_xoa_khoa_hoc.png)
## Technology
+ Front-end:
  :1.[Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-stated/introduction/)
  :2.Javascript
  :3.Jquery 3
  :4.Ajax
  :5.Local Storage
  :6.JSON
+ Back-end:
 : Spring boot with Hibernate
+ KIT:
 : [AdminLTE](https://adminlte.io/)
+ DB:
 : MySQL - PhpMyAdmi