var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function() {
    onPageLoading();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Ham xu ly su kien load trang
function onPageLoading() {
    getAllCourse();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Ham goi API lay danh sach course
function getAllCourse() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: 'get',
        data: 'json',
        success: function(res) {
            console.log(res);
            displayCoursePopular(res);
            displayCourseTrending(res);
        },
        error: function(err) {
            console.log(err);
        }
    })
}

// ham dinh nghia lai Popular Course
function displayCoursePopular(paramPopular) {
    for (let bI = 0; bI < paramPopular.length; bI++) {
        if(paramPopular[bI].isPopular === true && bI <= 5) {
            showCoursePopularCard(paramPopular[bI]);
            // console.log(paramPopular[bI])
        }
        
    }
}

// Ham dinh nghia lai Trending Course
function displayCourseTrending(paramTrending) {
    for (let bI = 0; bI < paramTrending.length; bI++) {
        if(paramTrending[bI].isTrending === true && bI <= 4) {
            console.log(paramTrending[bI]);
            showCourseTrendingCard(paramTrending[bI]);
        }
        
    }
}

// ham Ham show Popular Course

function showCoursePopularCard(paramPopular) {
    $("#card-popular").append(`
    <div class="card">
        <img src="${paramPopular.coverImage}" alt="...">
    <div class="card-body">
      <h5 class="card-name">${paramPopular.courseName}</h5>
      <div class="card-des d-flex mb-1">
        <div><i class="fa-regular fa-clock mr-1"></i></div>
        <div class="card-duration mr-1">${paramPopular.duration}</div>
        <div class="card-level">${paramPopular.level}</div>
      </div>
      <div class="card-price mb-2">
        <div class="price mr-2">$${paramPopular.discountPrice} </div>
        <div class="card-discount-price">$${paramPopular.price}</div>
      </div>
    </div>
    <div class="card-botton">
      <div><img src="${paramPopular.teacherPhoto}" class="card-photo"></div>
      <div class="teacher-name">${paramPopular.teacherName}</div>
      <div><i class="fa-regular fa-bookmark"></i></div>
    </div>
  </div>
    `);

}
// ham Ham show Trending Course

function showCourseTrendingCard(paramTrending) {
    $("#card-trending").append(`
    <div class="card">
        <img src="${paramTrending.coverImage}" alt="...">
    <div class="card-body">
      <h5 class="card-name">${paramTrending.courseName}</h5>
      <div class="card-des d-flex mb-1">
        <div><i class="fa-regular fa-clock mr-1"></i></div>
        <div class="card-duration mr-1">${paramTrending.duration}</div>
        <div class="card-level">${paramTrending.level}</div>
      </div>
      <div class="card-price mb-2">
        <div class="price mr-2">$${paramTrending.discountPrice} </div>
        <div class="card-discount-price">$${paramTrending.price}</div>
      </div>
    </div>
    <div class="card-botton">
      <div><img src="${paramTrending.teacherPhoto}" class="card-photo"></div>
      <div class="teacher-name">${paramTrending.teacherName}</div>
      <div><i class="fa-regular fa-bookmark"></i></div>
    </div>
  </div>
    `);

}